const GameState = Object.freeze({
    WELCOME: Symbol("welcome"),
	MOVIE: Symbol("movie"),
    BET: Symbol("bet"),
    TRAIL: Symbol("trail"),
	CALL: Symbol("call"),
    STRANGER: Symbol("stranger"),
	TEA: Symbol("tea"),
    ARTICLE: Symbol("article"),
	END: Symbol("end")
});

module.exports = class Game{
    constructor(){
        this.stateCur = GameState.WELCOME;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        switch(this.stateCur){
            case GameState.WELCOME:
				sReply = "You are going for a horror movie with your friends. inputs yes or no";
                this.stateCur = GameState.MOVIE;
                break;
				
			case GameState.MOVIE:
				if(sInput.toLowerCase().match("yes")){
					sReply = "It is a dark night. You have a BET that you can walk through a scary trail. Do you WAIT or GO through the scary trail? inputs wait or go";
					this.stateCur = GameState.BET;
				}	
				else{
					sReply = "If you donot go, then you will a looser and teased by your friends again. input yes or no?";
					this.stateCur = GameState.MOVIE;
				}
				break;
				
            case GameState.BET:
                if(sInput.toLowerCase().match("go")){
					sReply ="You started walking towards the scary trail. Do you walk ahead or run back to your friends? inputs walk or back";
                    this.stateCur = GameState.TRAIL;
                }
				else{
                    sReply = "Your friends are teasing you for being scared of darkness. Do you still think about it or go through weird trail?";
                    this.stateCur = GameState.BET;
                }
                break;
				
            case GameState.TRAIL:
                if(sInput.toLowerCase().match("walk")){
                    sReply = "You are walking through the scary trail and you have found a creepy man walking few feets ahead of you.Do you call him or your frinds for company? input as 'call' to call your friends.";
                    this.stateCur = GameState.CALL;
                }else{
                    sReply = "You are walking alone but its getting darker and frightening. Do you continue your walk alone or input as 'back' to go back?";
                    this.stateCur = GameState.END;
                }
                break;
			
			case GameState.CALL:
                if(sInput.toLowerCase().match("call")){
					sReply = "You are trying to call your friends but there is no network in your phone. Do you go with stranger? input yes or no";
					this.stateCur = GameState.STRANGER;
				}
				else{
					sReply ="Its getting more darker and you are afraid of darkness. Do you go back or not?";
					this.stateCur = GameState.END;
				}
				break;
				
            case GameState.STRANGER:
                if(sInput.toLowerCase().match("yes")){
                    sReply = "You called that stranger and started walking with him. He is living by the end of trail so he asked you for cup of tea at his house. DO you go for tea.input yes or no?";
                    this.stateCur = GameState.TEA;
                }else{
                    sReply = "You are really scared and want to go back to your friends. Do you go back or not?";
					this.stateCur = GameState.END;
                }
				break;
				
			case GameState.TEA:
                if(sInput.toLowerCase().match("yes")){
                    sReply = "You went to his house for tea and while serving tea you saw his face. You recognize that you read about him in the article and he was killed by a drug lord.? Do you run back or have tea?";
                    this.stateCur = GameState.ARTICLE;
                }else{
                    sReply = "You are really scared and want to go back to your friends. Do you go back or not?";
					this.stateCur = GameState.END;
                }
				break;	
				
			case GameState.ARTICLE:
                if(sInput.toLowerCase().match("tea")){
                    sReply = "You had tea. You have entered a new world of adventure. GAME OVER...";
                    this.stateCur = GameState.WELCOME;
				}		
				else{
					sReply = "You said no for tea and want to go home but he insist you to have a cup of tea.";
				}
				break;
				
			case GameState.END:
				if(sInput.toLowerCase().match("back")){
					sReply = "You are a looser, Game Over.";
				}
					
        }
        return([sReply]);
    }
}