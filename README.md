### INTRODUCTION ###

Scarry Story game, is an interactive user interface or chatbot application where you will have some questions and it will respond with the specified answers. It is a scarry story telling game. So, when user start this interface, they have to answer some questions and based on their answers they will get further options. At the end, either they will win or loss the game.

### TECHNOLOGY  ###

* Node JS
* JavaScript
* Text Editor


### REQUIRED SOFTWARE ###

* Visual Studio Code - https://code.visualstudio.com/download
* Node JS - https://nodejs.org/en/download/


### LICENCE ###
Copyright @HardeepKaur 2020.This application is a open source project where ANYONE CAN USE, IMPROVE IT AND CAN HAVE FULL ACCESS TO THE CODE WITHOUT ANY CONCERN OR COPYRIGHT ISSUES.